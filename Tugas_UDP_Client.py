# Sister IF-40-12
# Alan Maulana Ibrahim

# import library socket karena akan menggunakan IPC socket
import socket

# definisikan target IP server yang akan dituju
IP_Server = "192.168.43.155"

# definisikan target port number server yang akan dituju
Port_Server = 6004

# pesan yang akan dikirimkan
Pesan = "Implementasi UDP Stream"

#Menampilkan informasi IP, Port, dan pesan pada sisi client
print ("target IP:", IP_Server)
print ("target port:", Port_Server)
print ("pesan:", Pesan)

# buat socket bertipe UDP
socket = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)

# melakukan loop untuk mengirimkan pesan sebanyak 10 kali
for x in range (10):

 	# kirim pesan
 	x = socket.sendto(Pesan.encode(), (IP_Server,Port_Server))
