# Sister IF-40-12
# alan maulana ibrahim -1301154175

# import library socket karena menggunakan IPC socket
import socket

# definisikan IP server tujuan file akan diupload
IP_Server = "192.168.43.155"

# definisikan port number proses di server
Port_Server = 6004

# definisikan ukuran buffer untuk mengirim
Buffer_Size = 1024

# buat socket (apakah bertipe UDP atau TCP?)
socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# lakukan koneksi ke server
socket.connect((IP_Server,Port_Server))

# buka file bernama "file_diupload.txt bertipe byte
# masih hard code, file harus ada dalam folder yang sama dengan script python
file = open("file_diupload.txt", "rb")

try:
    # baca file tersebut sebesar buffer 
    byte = file.read(Buffer_Size)

    # selama tidak END OF FILE; pada pyhton EOF adalah b''
    while byte != b'':
        # kirim hasil pembacaan file
        socket.send(byte)
        
        # baca sisa file hingga EOF
        byte = file.read(Buffer_Size)
        #print(byte)

finally:
    print ("end sending")
    
    # tutup file jika semua file telah  dibaca
    file.close()

# tutup koneksi setelah file terkirim
socket.close()
