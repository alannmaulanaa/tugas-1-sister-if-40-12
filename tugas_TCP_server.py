# Sister IF-40-12
# Alan Maulana ibrahim

'''import library socket'''
import socket

'''alamat IP dan Port server yang digunakan untuk proses binding'''
IP_Server = "192.168.43.155"
Port_Server = 6004

Buffer_Size = 1024

socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

socket.bind((IP_Server,Port_Server))
socket.listen(5)

while 1:
	connect, address = socket.accept()
	print("Message from : ", address)

	data = connect.recv(Buffer_Size)
	print("Message : ", data.decode())

	data = "IP : " + str(address) + " your message has been procced. Please Wait"
	connect.send(data.encode())

connect.close()
