 Sister IF-40-12
# Alan Maulana ibrahim -1301154175

# import library socket karena menggunakan IPC socket
import socket

# definisikan IP untuk binding
IP_Server = "192.168.43.155"

# definisikan port untuk binding
Port_Server = 6004

# definisikan ukuran buffer untuk menerima pesan
Buffer_Size = 1024

# buat socket (bertipe UDP atau TCP?)
socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print('TCP Socket Berhasil Dibuat !')

# lakukan binding ke IP dan port
socket.bind((IP_Server, Port_Server))
print("Binding socket \nYour IP\t: " + IP_Server + "\nYour Port\t: " + str(Port_Server))


# lakukan listen
socket.listen(5)
print('Listening Socket.')

#  siap menerima koneksi
connect, address = socket.accept()
print("Connect from : ", address)


# buka file bernama "file_didownload.txt
file = open("file_didownload.txt", "rb")

# masih hard code, file harus ada dalam folder yang sama dengan script python


try:
    # baca file tersebut sebesar buffer 
    byte = file.read(Buffer_Size)
    print("File downloaded : \n", byte)
    print("\n\n total karakter file : \n", len(byte))
    
    # selama tidak END OF FILE; pada pyhton EOF adalah b''
    while byte != b'':

        # kirim hasil pembacaan file dari server ke client
        connect.send(byte)
        
        # baca sisa file hingga EOF
        byte = file.read(Buffer_Size)
        
finally:
    print ("end sending")
    
    # tutup file jika semua file telah  dibaca
    file.close()

# tutup socket
socket.close()

# tutup koneksi
connect.close()
