# Sister IF-40-12
# Alan Maulana ibrahim -1301154175

# import library socket karena menggunakan IPC socket
import socket

# definisikan IP untuk binding
IP_Server = "192.168.43.155"

# definisikan port untuk binding
Port_Server = 6004

# definisikan ukuran buffer untuk menerima pesan
Buffer_Size = 1024

# buat socket (bertipe UDP atau TCP?)
socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print('TCP Socket Berhasil Dibuat !')

# lakukan binding ke IP dan port
socket.bind((IP_Server, Port_Server))
print("Binding Socket \nYour Address\t: " + IP_Server + "\nYour Port\t: " + str(Port_Server))

# lakukan listen
socket.listen(5)

#  siap menerima koneksi
print('Listening socket.')

# buka/buat file bernama hasil_upload.txt untuk menyimpan hasil dari file yang dikirim server
# masih hardcoded nama file, bertipe byte



# loop forever
while 1:
    # terima pesan dari client
    connect, address = socket.accept()
    print("Message From : ", address)
    data = connect.recv(Buffer_Size)
    
    
    # tulis pesan yang diterima dari client ke file kita (result.txt)
    content = "{}".format(data)
    file = open("result.txt", "w")
    file.write(content)
    file.close()
    

    print("\n\nUploaded file with name result.txt \n")

    # berhenti jika sudah tidak ada pesan yang dikirim
    if not data: break
    
# tutup file result.txt    
file.close()

#tutup socket
socket.close()

# tutup koneksi
connect.close()
