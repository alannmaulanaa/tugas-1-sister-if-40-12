# Sister IF-40-12
# Alan Maulana ibrahim -1301154175

# import library socket karena menggunakan IPC socket
import socket

# definisikan IP server tujuan file akan diupload
IP_Server = "192.168.43.155"

# definisikan port number proses di server
PORT_Server = 6004

# definisikan ukuran buffer untuk mengirim
Buffer_Size = 1024

# buat socket (apakah bertipe UDP atau TCP? TCP)
socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# lakukan koneksi ke server
socket.connect((IP_Server, PORT_Server))
print("Holla Server!!!")


# buka file bernama "hasil_download.txt bertipe byte
# masih hard code, file harus ada dalam folder yang sama dengan script python
file = open("hasil_download.txt", "wb")
print("Sending. . .")

# loop forever
while 1:
    # terima pesan dari client
    data=socket.recv(Buffer_Size)
    print("Downloading.....")
    
    # tulis pesan yang diterima dari client ke file kita (result.txt)
    while (data):
    	file.write(data)
    	data = socket.recv(Buffer_Size)
    
    # berhenti jika sudah tidak ada pesan yang dikirim
    if not data: break
    
# tutup file_hasil_download.txt    
file.close()

#tutup socket
socket.close()
