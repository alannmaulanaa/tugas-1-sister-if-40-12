# Sister IF-40-12
# ALAN MAULANA IBRAHIM -1301154175

# import library socket karena akan menggunakan IPC socket
import socket

# definisikan tujuan IP server
IP_Server = "192.168.43.155" 

# definisikan port dari server yang akan terhubung
Port_Server = 6004 			

# definisikan ukuran buffer untuk mengirimkan pesan
Buffer_Size = 1024


# definisikan pesan yang akan disampaikan
Pesan = "Salah satu contoh pengimplementasian tcp stream"
print ("Target IP:", IP_Server)
print ("Target port:", Port_Server)
print ("Pesan :", Pesan)

# buat socket TCP
socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# lakukan koneksi ke server dengan parameter IP dan Port yang telah didefinisikan
socket.connect((IP_Server,Port_Server))

# kirim pesan ke server
socket.send(Pesan.encode())

# terima pesan dari server
receive_data = socket.recv(Buffer_Size)

# tampilkan pesan/reply dari server
print("Message from Server : ", receive_data.decode())

# tutup koneksi
socket.close()
